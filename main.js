class Employee {
    constructor(name,age,salary){
        this.name = name;
        this.age = age;
        this.salary = salary;
    }
    getName() {
        return this.name;
    }
    getAge() {
        return this.age;
    }
    getSalary() {
        return this.salary;
    }
    setName(newName){
        this.name = newName;
    }
    setAge(newAge){
        this.age = newAge;
    }
    setName(newSalary){
        this.salary = newSalary;
    }
}
class Programmer extends Employee{
    constructor(name,age,salary){
        super(name,age,salary);
        this.lang = ['JS','Java','C++'];
    }
    getSalary(){
       return this.salary * 3;
    }
}

const programmer1 = new Programmer('Vasia',20,4000);
const programmer2 = new Programmer('Petya',33,4400);
const programmer3 = new Programmer('Ebragim',54,3000);
const programmer4 = new Programmer('Georgiy',25,4500);
console.log(programmer1);
console.log(programmer2 );
console.log(programmer3 );
console.log(programmer4 );